import random
import string
import time

import uvicorn
from fastapi import FastAPI

from api import api_router
from service import Base
from service import engine

app = FastAPI()

app.include_router(api_router, prefix="/api/v1")


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        print("start migration database...")
        # await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)
        print("end migration database..")


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
