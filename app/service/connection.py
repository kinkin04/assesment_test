from sqlalchemy import MetaData
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import warnings
from sqlalchemy import exc as sa_exc

warnings.simplefilter("ignore", category=sa_exc.SAWarning)

# creates an instance of AsyncEngine which then offers an async version

POSTGRES_DB_HOST = "localhost"
# POSTGRES_DB_PORT=5432
POSTGRES_DB_EXPOSE_PORT = 5432
POSTGRES_DB_API = "assesment_be_python"
POSTGRES_DB_API_USER = "assesment_user"
POSTGRES_DB_API_PASSWORD = "assesment_password"


SQLALCHEMY_DATABASE_URL = f"postgresql+asyncpg://{POSTGRES_DB_API_USER}:{POSTGRES_DB_API_PASSWORD}@{POSTGRES_DB_HOST}:{POSTGRES_DB_EXPOSE_PORT}/{POSTGRES_DB_API}"
print(f"from connection : {SQLALCHEMY_DATABASE_URL}")


engine = create_async_engine(
    SQLALCHEMY_DATABASE_URL,
    echo=False,
    pool_size=100,
    max_overflow=200,
    pool_recycle=300,
    pool_pre_ping=True,
    pool_use_lifo=True,
)

# create AsyncSession sessionmaker version
async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)

metadata = MetaData()
Base = declarative_base(metadata=metadata)


# db async session
async def get_async_session():
    db = async_session()
    try:
        yield db
    finally:
        await db.close()
