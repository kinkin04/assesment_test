from datetime import datetime
from fastapi import Depends, APIRouter
from sqlalchemy.ext.asyncio import AsyncSession
from typing import Optional
from service import get_async_session
from schema import RegistAcc, postTransaction
from crud import account_crud

router = APIRouter()


@router.post("/daftar")
async def regist_acc(data: RegistAcc, db: AsyncSession = Depends(get_async_session)):
    out_resp = await account_crud.create_regist_acc(data=data, session=db)
    return out_resp


@router.post("/tabung")
async def deposit_balance(
    data: postTransaction, db: AsyncSession = Depends(get_async_session)
):
    out_resp = await account_crud.deposit_balance(data=data, session=db)
    return out_resp


@router.post("/tarik")
async def cash_withdrawal(
    data: postTransaction, db: AsyncSession = Depends(get_async_session)
):
    out_resp = await account_crud.cash_withdrawal(data=data, session=db)
    return out_resp


@router.get("/saldo/{no_rekening}")
async def balance(account_number: str, db: AsyncSession = Depends(get_async_session)):
    out_resp = await account_crud.balance_account(
        account_number=account_number, session=db
    )
    return out_resp


@router.get("/mutasi/{no_rekening}")
async def mutation(
    account_number: str,
    mutation: Optional[str] = None,
    db: AsyncSession = Depends(get_async_session),
):
    out_resp = await account_crud.mutation(
        account_number=account_number, mutation=mutation, session=db
    )
    return out_resp
